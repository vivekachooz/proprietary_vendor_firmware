LOCAL_PATH := $(call my-dir)

FOSTER_FIRMWARE := $(LOCAL_PATH)
TEGRAFLASH_PATH := $(BUILD_TOP)/vendor/nvidia/t210/r32/tegraflash
T210_BL         := $(BUILD_TOP)/vendor/nvidia/t210/r32/bootloader
FOSTER_BCT      := $(BUILD_TOP)/vendor/nvidia/foster/r32/BCT
JETSON_CV_FLASH := $(BUILD_TOP)/vendor/firmware/foster/jetson_cv
COMMON_FLASH    := $(BUILD_TOP)/device/nvidia/tegra-common/flash_package

TNSPEC_PY     := $(BUILD_TOP)/vendor/nvidia/common/rel-24/tegraflash/tnspec.py
FOSTER_TNSPEC := $(BUILD_TOP)/vendor/nvidia/foster/rel-30/tnspec/foster.json

INSTALLED_BMP_BLOB_TARGET      := $(PRODUCT_OUT)/bmp.blob
INSTALLED_KERNEL_TARGET        := $(PRODUCT_OUT)/kernel
INSTALLED_RECOVERYIMAGE_TARGET := $(PRODUCT_OUT)/recovery.img
INSTALLED_TOS_TARGET           := $(PRODUCT_OUT)/tos-mon-only.img

include $(CLEAR_VARS)
LOCAL_MODULE        := p2371_flash_package
LOCAL_MODULE_SUFFIX := .txz
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)

_p2371_package_intermediates := $(call intermediates-dir-for,$(LOCAL_MODULE_CLASS),$(LOCAL_MODULE))
_p2371_package_archive := $(_p2371_package_intermediates)/$(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)

$(_p2371_package_archive): $(INSTALLED_BMP_BLOB_TARGET) $(INSTALLED_KERNEL_TARGET) $(INSTALLED_RECOVERYIMAGE_TARGET) $(INSTALLED_TOS_TARGET)
	@mkdir -p $(dir $@)/tegraflash
	@mkdir -p $(dir $@)/scripts
	@cp $(TEGRAFLASH_PATH)/* $(dir $@)/tegraflash/
	@cp $(COMMON_FLASH)/*.sh $(dir $@)/scripts/
	@cp $(JETSON_CV_FLASH)/p2371.sh $(dir $@)/flash.sh
	@cp $(JETSON_CV_FLASH)/flash_t210_android_sdmmc_fb.xml $(dir $@)/
	@cp $(JETSON_CV_FLASH)/sign.xml $(dir $@)/
	@cp $(FOSTER_FIRMWARE)/foster_e/*.bin $(dir $@)/
	@cp $(FOSTER_FIRMWARE)/foster_e/rp4.blob $(dir $@)/
	@rm $(dir $@)/bpmp_zeroes.bin
	@cp $(T210_BL)/cboot.bin $(dir $@)/cboot_tegraflash.bin
	@cp $(T210_BL)/nvtboot_recovery.bin $(dir $@)/
	@cp $(INSTALLED_TOS_TARGET) $(dir $@)/
	@cp $(INSTALLED_BMP_BLOB_TARGET) $(dir $@)/
	@cp $(INSTALLED_RECOVERYIMAGE_TARGET) $(dir $@)/
	@cp $(KERNEL_OUT)/arch/arm64/boot/dts/tegra210-jetson-tx1-p2597-2180-a01-android-devkit.dtb $(dir $@)/
	@cp $(FOSTER_BCT)/P2180_A00_LP4_DSC_204Mhz.cfg $(dir $@)/
	@python2 $(TNSPEC_PY) nct new p2371-2180-devkit -o $(dir $@)/p2371-2180-devkit.bin --spec $(FOSTER_TNSPEC)
	@cd $(dir $@); tar -cJf $(abspath $@) *

include $(BUILD_SYSTEM)/base_rules.mk
